<?php

    function decodeProcess($string)
    {
        $hurufHidup = ['A','I','U','E','O'];
        $arrayString = str_split($string);
        foreach(array_values($arrayString) as $i => $value) {
            if (in_array($value, $hurufHidup)) {
                echo strtolower($value);
            } else {
                echo $value;
            }
        }
        echo "\n";
    }

    echo "Masukkan string anda : ";
    $inputString = fopen("php://stdin","r");
    $string = trim(fgets($inputString));
    $teksB = 'b';
    $teksP = 'p';
    $posisiB = strpos($string, $teksB);
    $posisiP = strpos($string, $teksP);

    // cek bila ada huruf p & b
    if ($posisiP === false && $posisiB === false || $posisiB === false && $posisiP === false || $posisiP !== false && $posisiB === false || $posisiB !== false && $posisiP === false)  {
        $string = $string;
    } else {
        $tmp = $posisiB;
        $posisiB = $posisiP;
        $posisiB = 'p';
        $posisiP = $tmp;
        $posisiP = 'b';

        $string = $posisiB."".$string."".$posisiP;

    }

    $result = decodeProcess($string);
    echo $result;

?>
